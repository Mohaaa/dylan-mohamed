#ifndef GENRE_H
#define GENRE_H

#include <iostream>
#include <string>


/** \class Genre
 *
*/
class Genre
{

  // Attributs

 private:
  int id;///< Object identifier
  std::string libelle;///< Genre name string

 public:


  // Méthodes
  // Constructeurs

/** \brief Default constructor
   *
   * With null #id and #name
   */

  Genre();

  /** \brief Detailed constructor
   *
   * Construct a #Genre object based on the detailed parameterization
   *
   * \param[in] _id The #Genre identification number
   *
   * \param[in] _name The #Genre name string
   */

  Genre(int, std::string);

/** \brief destructeur
*/

  ~Genre();
/** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */

  int getId();
  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */

  std::string getLibelle();
  /** \brief Set the object's #name
   *
   * \param[in] _name The \c std::string to use as #name
   */

  void setLibelle(std::string);


};


#endif// GENRE_H

