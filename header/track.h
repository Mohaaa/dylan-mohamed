#ifndef TRACK_H
#define TRACK_H
#include <iostream>

#include <vector>
#include "artist.h"
#include "album.h"
#include "polyphony.h"
#include "format.h"
#include "subgenre.h"
#include "genre.h"


/** \class Track
 */

class Track{

  //propriétés
private :
  unsigned int id;///< Object identifier
  unsigned int duration;///< Track duration in seconds
  std::string name;///< Track title string
  std::string path;///< Track path string
  std::vector<Artist>v_artist;///< Collection of associated Artist
  std::vector<Album> v_album;///< Collection of associated Album
  Polyphony polyphony;///< Polyphony of the current #Track
  Format format;///< Format of the current #Track
  Subgenre subgenre;///< SubGenre of the current #Track
  Genre genre;///< Genre of the current #Track

public :

/** \brief Default constructor
   *
   * With null #id, #title, #duration and #path
   */
  Track();

  /** \brief Detailed constructor
   *
   * Construct a #Track object based on the detailed parameterization
   *
   * \param[in] _id The #Track identification number
   *
   * \param[in] _title The #Track title string
   *
   * \param[in] _duration The #Track duration, in seconds
   *
   * \param[in] _path The #Track file path
   *
   * \param[in] _artist The #Track first Artist collection element
   *
   * \param[in] _album The #Track first Album collection element
   *
   * \param[in] _format The #Track Format
   *
   * \param[in] _genre The #Track Genre
   *
   * \param[in] _subgenre The #Track SubGenre
   *
   * \param[in] _polyphony The #Track Polyphony
   */

  Track(unsigned int _id,
        unsigned int _duration,
        std::string name,
        std::string path,
        std::vector<Artist> v_artist,
        std::vector<Album> v_album,
        Polyphony polyphony,
        Format format,
        Subgenre subgenre,
        Genre genre);

        /** \brief destructeur
        */
  ~Track();

  void setId(unsigned int);
  void setDuration(unsigned int);
  void setName(std::string);
  void setPath(std::string);
  void setArtist(std::vector<Artist>);
  void setAlbum(std::vector<Album>);
  void setPolyphony(Polyphony);
  void setFormat(Format);
  void setSubgenre(Subgenre);
  void setGenre(Genre);

  unsigned int getId();
  unsigned int getDuration();
  std::string getPath();
  std::string getName();
  std::vector<Artist> getArtist();
  std::vector<Album> getAlbum();
  Genre getGenre();
  Subgenre getSubgenre();
  Polyphony getPolyphony();
  Format getFormat();


};
#endif
