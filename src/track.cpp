#include <iostream>
#include "../header/track.h"

using namespace std;

// Implémentation du constructeur par défaut

Track::Track():
  id(0),
  duration(0),
  name(""),
  path(""),
  v_artist(),
  v_album(),
  polyphony(),
  format(),
  subgenre(),
  genre()

{
  cout<<"Création d'une piste";
}

Track::Track(unsigned int _id,
             int _duration,
             std::string name,
             std::string path,
             std::vector<Artist>v_artist,
             std::vector<Album>v_album,
             Polyphony polyphony,
             Format format,
             Subgenre subgenre,
             Genre genre):

id(_id),
duration(_duration),
name(name),
path(path),
v_artist(v_artist),
v_album(v_album),
polyphny(polyphony),
format(format),
subgenre(subgenre),
genre(genre)

{
  cout <<"Création d'une track avec des paramètres";
}

//destructeur
Track::~Track(){};


unsigned int Track::getId(){
  return id;
}


unsigned int Track::getDuration(){
  return duration;
  }

std::string Track::getName(){
  return name;
}

std::string Track::getPath(){

  return path;
}

std::vector<Artist> Track::getArtist(){
  return v_artist;
}

std::vector<Album> Track::getAlbum(){
  return v_album;
}
